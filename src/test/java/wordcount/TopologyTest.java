package wordcount;

import backtype.storm.Config;
import backtype.storm.ILocalCluster;
import backtype.storm.Testing;
import backtype.storm.generated.StormTopology;
import backtype.storm.testing.CompleteTopologyParam;
import backtype.storm.testing.MkClusterParam;
import backtype.storm.testing.MockedSources;
import backtype.storm.testing.TestJob;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import helloworld.HelloWorldBolt;
import helloworld.HelloWorldSpout;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by nnombela on 3/18/14.
 */
public class TopologyTest {
    @Test
    public void verifyProperValuesAreEmittedByEachBolt() {
        MkClusterParam clusterParam = new MkClusterParam();
        clusterParam.setSupervisors(1);

        Config daemonConf = new Config();
        daemonConf.setDebug(false);
        clusterParam.setDaemonConf(daemonConf);

        Testing.withSimulatedTimeLocalCluster(clusterParam, new TestJob() {
            @Override
            public void run(ILocalCluster cluster) {
                Config config = new Config();
                config.setDebug(true);

                MockedSources mockedSources = new MockedSources();
                Values expectedSentence = new Values("verify proper values are emitted");
                List expectedWords = new Values(new Values("proper"), new Values("verify"), new Values("values"), new Values("are"), new Values("emitted"));

                mockedSources.addMockData("sentence-spout", expectedSentence);

                CompleteTopologyParam topologyParam = new CompleteTopologyParam();
                topologyParam.setMockedSources(mockedSources);
                topologyParam.setStormConf(config);

                TopologyBuilder builder = WordCountTopology.createTopologyBuilder();
                StormTopology topology = builder.createTopology();

                Map result = Testing.completeTopology(cluster, topology, topologyParam);

                Values sentence = (Values)Testing.readTuples(result, "sentence-spout").get(0);

                // Assert emitted tuple out of the spout are the expected ones
                assertTrue(Testing.multiseteq(expectedSentence, sentence));

                // Assert emitted tuples out of the  bolt are the expected ones
                List words = Testing.readTuples(result, "split-sentence-bolt");
                assertTrue(Testing.multiseteq(expectedWords, words));
            }
        });
    }
}
