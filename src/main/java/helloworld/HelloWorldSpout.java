package helloworld;

import java.util.Map;
import java.util.Random;

import backtype.storm.Config;
import backtype.storm.metric.api.CountMetric;
import org.apache.log4j.Logger;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

/**
 * Created by nnombela on 3/16/14.
 */
public class HelloWorldSpout extends BaseRichSpout {
    public static Logger LOG = Logger.getLogger(HelloWorldSpout.class);

    private static final int MAX_RANDOM = 10;

    private SpoutOutputCollector collector;
    private transient CountMetric numTuples;

    public HelloWorldSpout() {
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        //Integer bucketSize = (Integer)conf.get(Config.TOPOLOGY_BUILTIN_METRICS_BUCKET_SIZE_SECS);
        this.numTuples = context.registerMetric("wordcount", new CountMetric(), 2);
    }

    @Override
    public void nextTuple() {
        Utils.sleep(100);

        int rnd = new Random().nextInt(MAX_RANDOM);

        collector.emit(new Values("Hello World " + rnd));


        numTuples.incr();
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("sentence"));
    }

}
