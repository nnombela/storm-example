package helloworld;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.metric.LoggingMetricsConsumer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;

/**
 * Created by nnombela on 3/16/14.
 */
public class HelloWorldTopology {

    public static void main(String[] args) throws Exception {
        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("randomHelloWorld", new HelloWorldSpout(), 2);

        builder.setBolt("HelloWorld1Bolt", new HelloWorldBolt(1), 4)
                .fieldsGrouping("randomHelloWorld", new Fields("sentence"));

//        builder.setBolt("HelloWorld2Bolt", new HelloWorldBolt(2), 2)
//                .shuffleGrouping("randomHelloWorld");

        Config conf = new Config();
        conf.setDebug(false);
        conf.registerMetricsConsumer(LoggingMetricsConsumer.class, 1);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("test", conf, builder.createTopology());

        Utils.sleep(5*1000);
        cluster.killTopology("test");
        cluster.shutdown();

    }

}