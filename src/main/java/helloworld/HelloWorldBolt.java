package helloworld;

import java.util.Map;
import org.apache.log4j.Logger;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

/**
 * Created by nnombela on 3/16/14.
 */
public class HelloWorldBolt extends BaseRichBolt {
    public static Logger LOG = Logger.getLogger(HelloWorldBolt.class);

    private int myCount = 0;

    private final int number;

    public HelloWorldBolt(int number) {
        this.number = number;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
    }

    @Override
    public void execute(Tuple input) {
        String test = input.getStringByField("sentence");

        if(test.equals("Hello World " + number)){
            myCount++;
            LOG.warn("Found a Hello World " + number + "! My Count is now: " + myCount);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("myCount"));
    }

}

