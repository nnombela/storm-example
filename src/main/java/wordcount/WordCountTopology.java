package wordcount;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;


/**
 * Created by nnombela on 3/18/14.
 */
public class WordCountTopology {


    public static void main(String[] args) throws Exception {
        Config conf = new Config();
        conf.setDebug(false);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("test", conf, createTopologyBuilder().createTopology());

        Utils.sleep(4 * 1000);
        cluster.killTopology("test");
        cluster.shutdown();
    }

    public static TopologyBuilder createTopologyBuilder() {
        SentenceSpout spout = new SentenceSpout(
                "the cow jumped over the moon",
                "the man went to the store and bought some candy",
                "four score and seven years ago",
                "how many apples can you eat");
        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("sentence-spout", spout, 1);
        builder.setBolt("split-sentence-bolt", new SplitSentenceBolt(), 3).shuffleGrouping("sentence-spout");
        builder.setBolt("word-counter-bolt", new WordCounterBolt(), 2).fieldsGrouping("split-sentence-bolt", new Fields("word"));

        return builder;
    }

}
