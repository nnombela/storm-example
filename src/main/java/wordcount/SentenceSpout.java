package wordcount;

import backtype.storm.Config;
import backtype.storm.metric.api.CountMetric;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import org.apache.log4j.Logger;

import java.util.Map;

/**
 * Created by nnombela on 3/18/14.
 */
public class SentenceSpout extends BaseRichSpout {
    public static Logger LOG = Logger.getLogger(SentenceSpout.class);

    final String[] outputs;
    int index = -1;

    int transactionId = 0;

    SpoutOutputCollector collector;

    public SentenceSpout(String... outputs) {
        this.outputs = outputs;
    }


    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("sentence"));
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void nextTuple() {
        this.index++;
        if (this.index >= outputs.length) {
            this.index = 0;
            Utils.sleep(1000);

        }
        collector.emit(new Values(outputs[index]), ++transactionId);
        System.out.println("######## Emitting TransactionId " + transactionId);
    }

    @Override
    public void ack(Object msgId) {
        System.out.println("######## Acknowledge received, msgId " + msgId);
    }

    @Override
    public void fail(Object msgId) {
        System.out.println("######## Fail received, msgId " + msgId);
    }

}
