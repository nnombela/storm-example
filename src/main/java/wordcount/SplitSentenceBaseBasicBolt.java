package wordcount;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/**
 * Created by nnombela on 3/18/14.
 *
 * By using BaseBasicBolt we avoid to anchor tuples and acknowledge them
 */
public class SplitSentenceBaseBasicBolt extends BaseBasicBolt {

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("word"));
    }

    @Override
    public void execute(Tuple input, BasicOutputCollector collector) {
        for(String word: input.getString(0).split(" ")) {
            if(!word.trim().isEmpty()) {
                collector.emit(new Values(word.toLowerCase()));
            }
        }
    }
}
