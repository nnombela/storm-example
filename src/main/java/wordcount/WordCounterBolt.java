package wordcount;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by nnombela on 3/18/14.
 */
public class WordCounterBolt extends BaseRichBolt {
    public static Logger LOG = Logger.getLogger(WordCounterBolt.class);

    private static final int MAX_RANDOM = 20;

    Integer id;
    String name;

    OutputCollector collector;
    Map<String, Integer> counters = new HashMap<String, Integer>();

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
        this.name = context.getThisComponentId();
        this.id = context.getThisTaskId();
    }

    @Override
    public void execute(Tuple input) {
        String word = input.getString(0);

        if (new Random().nextInt(MAX_RANDOM) == 1) { // vamos a fallar de vez en cuando
            System.out.println("######## Failing for input " + input);
            collector.fail(input);
            return;
        }

        int count = counters.containsKey(word)? counters.get(word) : 0;
        counters.put(word, ++count);

        collector.ack(input);
    }

    @Override
    public void cleanup() {
        System.out.println("######## -- Word Counter [" + name + "-" + id + "] --");
        for(Map.Entry<String, Integer> entry : counters.entrySet()){
            System.out.println("######## " + entry.getKey() + ": " + entry.getValue());
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }


}
